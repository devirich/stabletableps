function New-StableTable {
    [CmdletBinding()]
    param (
        # Where to store the table
        $Path,
        [switch]$SkipIndexing
        # InMemoryIndex
    )

    $Path = Resolve-Path_Force $Path
    $table = [StableTable]@{ Path = $Path }
    if (-not $PSBoundParameters.SkipIndexing -and (Test-Path $Path)) {
        $table.UpdateIndex()
    }
    $table
}

# StableTablePS

The PowerShell Stable Table module enables the use of large files to be used instead of an in-memory hashtable.

## Description
StableTablePS provides fast access to a hard drive hashtable-like table. It is built to enable easy get/set/add to arbitrarily large files without consuming large amounts of RAM.

You can think of StableTablePS kind of like a NoSql table store or hashtable.

## Installation

```
Install-Module StableTablePS -Scope Currentuser
```

## Usage
Target a table like so for creation:
```
$table = New-StableTable foo.txt
```
There are two main ways to add data to the table: Set and BulkSet.

Set allows you to specify a key and value to store to the table:
```
$table.Set("myKey",234)
$table.Set("myOtherKey",([PSCustomObject]@{foo="Carrots"}))
```

 BulkSet allows you to drop a hashtable directly to the module for writing.
```
$ht = @{
    myKey = 23456
    myOtherKey = [PSCustomObject]@{foo="Hammers"}
}
$table.BulkSet($ht)
```

Once you've created some data, you may want to re-index the file:
```
$table.CreateIndex()
```

And then query it:
```
$table.Get("myKey")
```
```
23456
```

And
```
$table.Get("myOtherKey")
```
```
foo
---
Hammers
```

You can even query multiple keys at once:
```
$keys = "myKey","myOtherKey"
$table.Get($keys)
```
```
Name                           Value
----                           -----
myKey                          23456
myOtherKey                     @{foo=Hammers}
```

## Roadmap
* Support leaving the index on disk
* Add UpdateIndex() to enable partion updates to an index
* Set tablename as last read position for partion updates
* Add the clean file method to remove old copies of each key


## Contributing
Open a PR or raise an issue!
